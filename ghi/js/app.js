function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <p class="card-text"><small class="text-muted">${start} - ${end}</small></p>
        
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
    
        if (!response.ok) {
          const column = document.querySelector('#column-1')
          column.innerHTML += showError()
        } else {
          let x = 0
          const data = await response.json();
          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const description = details.conference.description;
                  const name = details.conference.name;
                  const pictureUrl = details.conference.location.picture_url;
                  const starts = new Date(details.conference.starts)
                  const start = starts.toLocaleDateString("en-US");
                  const ends = new Date(details.conference.ends)
                  const end = ends.toLocaleDateString("en-US");
                  const location = details.conference.location.name;
                  const indexHTML = createCard(name, description, pictureUrl,start, end, location)
                  const column = document.querySelector(`.tag${x % 3}`);
                  x++;
                  column.innerHTML += indexHTML;              
          }
          }  
        }
    } catch (e) {
        alert(e)
        // Figure out what to do if an error is raised
    }
});


function showError() {
  return `<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>`
}